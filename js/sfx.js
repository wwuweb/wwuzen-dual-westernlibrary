// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - http://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document, undefined) {

Drupal.behaviors.parentSiteNameDual = {
		attach: function () {

		// Gets copy from site name (ultimately inside a span: #site-name a span)
		var parentName = $(".parent-site-header").text();

		var parentNameHuxley = parentName.replace("of the", "<span class=\"diminutive-type\">of the</span>");
		$('.parent-site-header span:contains(Huxley)').replaceWith(parentNameHuxley);

		// Rules for different colleges. Watch out for "&" (or &amp;) versus "and" use.
		/*var parentNameCST = parentName.replace("College of Science and Technology", "<span class=\"diminutive-type\">College of</span> Science <span class=\"diminutive-type\">and</span> Technology");
		$('.parent-site-header span:contains(Science and Technology)').replaceWith(parentNameCST);

		var parentNameCHSS = parentName.replace("College of", "<span class=\"diminutive-type\">College of</span>");
		$('.parent-site-header span:contains(Social Sciences)').replaceWith(parentNameCHSS);

		var parentNameCFPA = parentName.replace("College of Fine and Performing Arts", "<span class=\"diminutive-type\">College of</span> Fine <span class=\"diminutive-type\">and</span> Performing Arts");
		$('.parent-site-header span:contains(Performing Arts)').replaceWith(parentNameCFPA);*/
	}
}

//START accordion menu accessibility workaround
  Drupal.behaviors.accordionAccessibility = {
    attach: function (context) {
      function accordionMenuConvert($root) {
        var $ul = $root.children('ul').clone(false);

        if (!$ul.length) {
          $ul = $('<ul>', { 'class': 'menu' });

          $root.children('h3').each(function (index, element) {
            var $h3 = $(element);
            var $div = $h3.next('div');
            var $submenu = accordionMenuConvert($div);
            var $li = $('<li>', { 'class': 'menu__item' });
            var $a = $h3.children('.accordion-link').clone(false);

            if ($a.length) {
              $li.html($a.removeClass('accordion-link').addClass('menu__link'));
            }

            if ($submenu.children().length) {
              $li.append($submenu);
            }
            else {
              $li.addClass('is-leaf').addClass('leaf');
            }

            $ul.append($li);
          });

          $ul.children().first().addClass('first');
          $ul.children().last().addClass('last');
        }

        return $ul;
      }

      $('[class^="accordion-menu-"]', context).each(function () {
        var $accordion = $(this);

        var $enableAlert = $accordion.siblings('.enable-menu-alert');
        var $disableAlert = $accordion.siblings('.disable-menu-alert');

        var $enableAlertButton = $enableAlert.find('.enable-accordion');
        var $disableAlertButton = $disableAlert.find('.menu-disable-button');

        var $menuWarning = $disableAlert.find('.menu-enabled');
        var $menuDisabled = $enableAlert.find('.menu-disabled');

        var $accessibleMenu = accordionMenuConvert($accordion);

        function accordionMenuDisable() {
          $disableAlert.hide();
          $accordion.hide();
          $accordion.addClass('accordion-menu-disabled');
          $accordion.removeClass('accordion-menu-enabled');
          $accordion.children('a').attr('tabindex', '-1');
          $accessibleMenu.show();
          $enableAlert.show();
        }

        function accordionMenuEnable() {
          $enableAlert.hide();
          $accordion.show();
          $accordion.addClass('accordion-menu-enabled');
          $accordion.removeClass('accordion-menu-disabled');
          $accordion.children('a').removeAttr('tabindex');
          $accessibleMenu.hide();
          $disableAlert.show();
        }

        function checkLocalStorage() {
          if (localStorage.getItem('accordiondisabled') === 'true') {
            accordionMenuDisable();
          }
          else {
            accordionMenuEnable();
          }
        }

        function navWrapper () {
          var $accordionDisabled = $('.accordion-menu-disabled');
          var $accessibleMenu = $accordionDisabled.siblings('ul', { 'class': 'menu' });

          if ($accordionDisabled.length) {
            $accessibleMenu.wrapAll("<nav aria-label='Submenu'></nav>");
          }
          else {
            return false;
          }
        }

        $accordion.after($accessibleMenu.hide());
        checkLocalStorage();

        $disableAlertButton.click(function() {
          localStorage.setItem('accordiondisabled', 'true');
          accordionMenuDisable();
          navWrapper();
          $menuDisabled.focus();
          ga('send', 'event', { eventCategory: 'Accessibility', eventAction: 'Enable/Disable', eventLabel: 'Interior Menu Fix'});
        });

        $enableAlertButton.click(function() {
          localStorage.setItem('accordiondisabled', 'false');
          accordionMenuEnable();
          navWrapper();
          $menuWarning.focus();
        });
      });
    }
  };
  // END accordion menu accessibility workaround


})(jQuery, Drupal, this, this.document);


