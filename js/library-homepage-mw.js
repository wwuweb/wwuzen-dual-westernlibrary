
/* David Bass - July 2015 - homepage tabs for OneSearch */
up.jQuery(document).ready(function() {
	up.jQuery("li.tab").click(function() {
		up.jQuery("li").removeClass("selected");
		up.jQuery(this).addClass("selected");
	});


        up.jQuery("li.tab")
              .mouseenter(function() {
                var thisId = up.jQuery(this).attr("id");
                var tabtipId = "#" + thisId + "_tip";
                up.jQuery(tabtipId).show();
              })
              .mouseleave(function() {
                var thisId = up.jQuery(this).attr("id");
                var tabtipId = "#" + thisId + "_tip";
                up.jQuery(tabtipId).hide();
              });

	up.jQuery("#submit-to-primo").bind("submit", function(event) {
	 	event.preventDefault();
		searchPrimo();
	});


	up.jQuery("#signinLink").click(function(event) {
	 	event.preventDefault();
		searchPrimo(true);
	});


	function searchPrimo(loginPrompt) {
	 	var preSearch = "";
	 	var query = "";
	 	var searchURL = "";

	 	if (loginPrompt) {
	  		// if the end-user clicked on the "Sign-in" link, then let's run this function, but send them to the login-prompt page first
		     	preSearch = "https://onesearch-pds.library.wwu.edu/pds?func=load-login&institute=WWU&calling_system=primo&lang=eng&url=http://onesearch.library.wwu.edu/primo_library/libweb/action/login.do?loginFn=signin&vid=WWU&targetURL=";
		  }

		  var tempQueryValue = up.jQuery("#search_field").val();

		  // do not submit if query is empty, unless the SignIn link was clicked
		  if ((loginPrompt != true) && (tempQueryValue == "")) {
		  	return false;
		  }

		var selectedTabScope = up.jQuery("li.tab.selected span").data("scope");
		var selectedTabTab = up.jQuery("li.tab.selected span").data("tab");
		searchURL = "http://onesearch.library.wwu.edu/primo_library/libweb/action/dlSearch.do?vid=WWU&mode=Basic&search_scope=" + selectedTabScope + "&tab=" + selectedTabTab + "&institution=01ALLIANCE_WWU" ;

		if (tempQueryValue != "") {
			var tempQueryValueSimple = tempQueryValue.replace(/'/g,"\'");
			tempQueryValueSimple = tempQueryValueSimple.replace(/,/g,' ');
			query = "&query=any,contains," + tempQueryValueSimple;
		} else {
			query = "&query=";
		}

		searchURL +=  query;

		console.log(searchURL);
		var finalUrl =  preSearch + searchURL;
		location.href = finalUrl;
		return false;
	}

});


/*
document.addEventListener("DOMContentLoaded", function() {			// this is the plain Vanilla equivalent of document.ready in up.jQuery
	document.getElementById('signinLink').onclick = function() {
		// when the sign-in link is clicked, run the search function;
		searchPrimo(true);
	    	return false;
	};
});
*/